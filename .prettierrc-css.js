// .prettierrc-css.js
module.exports = {
    'tabWidth': 4,
    'singleQuote': true,
    'parser': 'scss',
}
