import * as React from 'react';
import svgProps, {IIconProps} from './svgProps';

export default function AppsIcon(props: IIconProps = {}) {
    /* tslint:disable:max-line-length */
    return (
        <svg {...svgProps} {...props}>
            <path d="M4 8h4V4H4v4zm6 12h4v-4h-4v4zm-6 0h4v-4H4v4zm0-6h4v-4H4v4zm6 0h4v-4h-4v4zm6-10v4h4V4h-4zm-6 4h4V4h-4v4zm6 6h4v-4h-4v4zm0 6h4v-4h-4v4z" />
            <path d="M0 0h24v24H0z" fill="none" />
        </svg>
    );
    /* tslint:enable */
}
