import AppsIcon from './AppsIcon';
import BackIcon from './BackIcon';
import LockIcon from './LockIcon';
import MailIcon from './MailIcon';

export {AppsIcon, BackIcon, MailIcon, LockIcon};
