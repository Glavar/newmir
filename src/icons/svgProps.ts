export default {
    fill: '#fff',
    stroke: 'none',
    viewBox: '0 0 24 24',
    xmlns: 'http://www.w3.org/2000/svg',
};

export interface IIconProps {
    className?: any;
}
