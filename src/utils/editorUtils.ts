// import {ICropTextInNodesInterface} from '_component/TextEditorComponent/TextEditorComponent.interface';
import {ISelection} from '_stores/TextStore/TextStore.interface';

// const PADDING_BOTTOM_TEXT = 25;
// const TEXT_QUERY_SELECTOR = '[data-text]';
const BLOCK_ATTRIBUTE_NAME = 'data-offset-key';
const BLOCK_REPLACE_NAME = '(-.)+';

function getBlockKey(element: Node): string {
    let elementNode = element as HTMLElement;
    while (!elementNode.hasAttribute || !elementNode.hasAttribute(BLOCK_ATTRIBUTE_NAME)) {
        elementNode = elementNode.parentNode as HTMLElement;
    }
    return elementNode.getAttribute(BLOCK_ATTRIBUTE_NAME).replace(new RegExp(BLOCK_REPLACE_NAME), '');
}

export function getEditorSelection(): ISelection {
    const {startContainer, startOffset, endContainer, endOffset} = window.getSelection().getRangeAt(0);
    return {
        anchorKey: getBlockKey(startContainer),
        anchorOffset: startOffset,
        focusKey: getBlockKey(endContainer),
        focusOffset: endOffset,
    };
}

// Last

// export function isBlockOut(element: HTMLElement, parentSize: ClientRect) {
//     const {bottom} = element.getBoundingClientRect();
//     const maxParentPosition = parentSize.bottom - PADDING_BOTTOM_TEXT;
//     return maxParentPosition < bottom;
// }
//
// export function getEditorNodes(editorContainer): HTMLElement[] {
//     let elementsBlock = editorContainer.children;
//     while (!elementsBlock[0].hasAttribute('data-block')) {
//         elementsBlock = elementsBlock[0].children;
//     }
//     return elementsBlock;
// }
//
// export function cropTextInNodes({indexesBlocks, elements, textState, parentSize}: ICropTextInNodesInterface): any {
//     const currentContent = textState.getCurrentContent();
//     const textBlocks = currentContent.getBlocksAsArray();
//     const textForNewPage = [];
//     let lastIndex = indexesBlocks.shift();
//     while (indexesBlocks.length) {
//         textForNewPage.unshift(textBlocks[lastIndex].getText());
//         lastIndex = indexesBlocks.shift();
//     }
//     const wordsOut = [];
//     const currentElement = elements[lastIndex];
//     const textElement = currentElement.querySelector(TEXT_QUERY_SELECTOR) as HTMLElement;
//     const textArray = textBlocks[lastIndex].getText().split(' ');
//     let lastWord = textArray.pop();
//     while (lastWord && isBlockOut(textElement, parentSize)) {
//         textElement.innerText = textArray.join(' ');
//         wordsOut.unshift(lastWord);
//         lastWord = textArray.pop();
//     }
//     textForNewPage.unshift(wordsOut.join(' '));
//     const newText = textForNewPage.join('\n');
//     let currentText = currentContent.getPlainText();
//     currentText = currentText.substr(0, currentText.length - newText.length);
//     return {
//         currentText,
//         newText,
//     };
// }
//
// export function findOutText(elements: HTMLElement[], parentSize: ClientRect) {
//     let currentElementIndex = elements.length - 1;
//     const blocksOut = [];
//
//     while (currentElementIndex) {
//         const element = elements[currentElementIndex];
//         const {top, bottom} = element.getBoundingClientRect();
//         const maxParentPosition = parentSize.bottom - PADDING_BOTTOM_TEXT;
//         const isTextOut = maxParentPosition < bottom;
//         if (!isTextOut && maxParentPosition > top) {
//             break;
//         }
//         blocksOut.push(currentElementIndex);
//         currentElementIndex--;
//     }
//
//     return blocksOut;
// }
