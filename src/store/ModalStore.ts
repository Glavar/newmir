import {computed, observable} from 'mobx';

export class ModalStoreModel {
    @observable public activeLoginModal: boolean = false;

    public showLoginModal = () => {
        this.activeLoginModal = true;
    }

    public hideLoginModal = () => {
        this.activeLoginModal = false;
    }

    @computed
    public get isLoginModalActive() {
        return this.activeLoginModal;
    }
}

export default new ModalStoreModel();
