import {computed, observable} from 'mobx';

export class LoginStoreModel {
    @observable public activeLogin: boolean = true;
    @observable public activeEarlyAccess: boolean = false;

    public showLogin = () => {
        this.activeLogin = true;
        this.activeEarlyAccess = false;
    }

    public showEarlyAccess = () => {
        this.activeLogin = false;
        this.activeEarlyAccess = true;
    }

    @computed
    public get isLoginActive() {
        return this.activeLogin;
    }

    @computed
    public get isEarlyAccessActive() {
        return this.activeEarlyAccess;
    }
}

export default new LoginStoreModel();
