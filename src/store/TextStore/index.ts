import {getEditorSelection} from '_util/editorUtils';
import makeID from '_util/makeID';
import {ContentState, convertFromRaw, convertToRaw, EditorState, SelectionState} from 'draft-js';
import {action, computed, observable} from 'mobx';

const Text = Array(50)
    .fill('Hello lorem pixel text\n')
    .join('');

export class TextStore {
    @observable private pagesTextStore;
    @observable private projectTextStore;

    constructor() {
        this.projectTextStore = EditorState.createWithContent(ContentState.createFromText(Text));
        this.pagesTextStore = [{text: this.projectTextStore, key: makeID()}];
    }

    // @action('add new page')
    // public pushToNextPage = ({newText}, index = 0) => {
    //     // this.textStore[index].text = EditorState.createWithContent(ContentState.createFromText(currentText));
    //     this.textStore[index + 1] = {
    //         key: makeID(),
    //         text: EditorState.createWithContent(ContentState.createFromText(newText)),
    //     };
    // }

    @action('save project text to store')
    public setSelection = () => {
        const selection = new SelectionState({
            ...getEditorSelection(),
            hasFocus: true,
        });
        this.projectTextStore = EditorState.forceSelection(this.projectTextStore, selection);
    }

    @action('save project text to store')
    public setProjectTextState = (textState) => {
        this.projectTextStore = textState;
        const rawDraftContentState = convertToRaw(textState.getCurrentContent());
        const contentState = EditorState.createWithContent(convertFromRaw(rawDraftContentState));
        if (typeof this.pagesTextStore[0] === 'undefined') {
            return this.pagesTextStore.push({text: contentState, key: makeID()});
        }
        this.pagesTextStore[0].text = contentState;
    }

    @computed
    public get projectTextState() {
        return this.projectTextStore;
    }

    @computed
    public get pagesTextState() {
        return this.pagesTextStore;
    }
}

export default new TextStore();
