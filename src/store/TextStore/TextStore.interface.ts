export interface ISelection {
    anchorKey: string;
    focusKey: string;
    anchorOffset: number;
    focusOffset: number;
}
