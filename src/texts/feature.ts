import {feature1Image} from '_image';

export const FeaturesMainSection = [
    {
        blocks: [
            {
                description: `High level Security 2-factor authentication, advanced encryption technology,
                cold storage - all that to give you stress and risk free trading experience`,
                image: feature1Image,
                title: 'Safety',
            },
            {
                description: 'Multitraders offers a powerful and simple API solution',
                image: feature1Image,
                title: 'API',
            },
            {
                description: 'Secured wallet, different currencies in one account, instant deposit and withdraw',
                image: feature1Image,
                title: 'Cryptocurrency wallet',
            },
        ],
    },
    {
        blocks: [
            {
                description: 'Most popular cryptocurrencies in one place, Bitcoin and many more',
                image: feature1Image,
                title: 'Multi coin support',
            },
            {
                description: 'Wide variety of payment methods deposit to an account',
                image: feature1Image,
                title: 'Payment options',
            },
            {
                description: '2% fees',
                image: feature1Image,
                title: 'Reasonable commissions',
            },
        ],
    },
];
