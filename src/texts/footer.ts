import makeID from '_util/makeID';

export const footerNavText = [
    {
        key: makeID(),
        list: [
            {
                key: makeID(),
                title: 'Buy Bitcoin',
            },
            {
                key: makeID(),
                title: 'Buy Ethereum',
            },
            {
                key: makeID(),
                title: 'Sell Bitcoin',
            },
            {
                key: makeID(),
                title: 'Crypto Trading',
            },
        ],
        title: 'Services',
    },
    {
        key: makeID(),
        list: [
            {
                key: makeID(),
                title: 'Buy Bitcoin',
            },
            {
                key: makeID(),
                title: 'Buy Ethereum',
            },
            {
                key: makeID(),
                title: 'Sell Bitcoin',
            },
            {
                key: makeID(),
                title: 'Crypto Trading',
            },
        ],
        title: 'Services',
    },
    {
        key: makeID(),
        list: [
            {
                key: makeID(),
                title: 'Buy Bitcoin',
            },
            {
                key: makeID(),
                title: 'Buy Ethereum',
            },
            {
                key: makeID(),
                title: 'Sell Bitcoin',
            },
            {
                key: makeID(),
                title: 'Crypto Trading',
            },
        ],
        title: 'Services',
    },
];

export const copyrightText = '©2018 - MultiTraders. All right reserved';
