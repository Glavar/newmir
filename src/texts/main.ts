import {pressImage} from '_image';
import makeID from '_util/makeID';

export const digitalItemsText = [
    {
        description: 'In digital Currency Exchange',
        key: makeID(),
        title: '70B+',
    },
    {
        description: 'In digital Currency Exchange',
        key: makeID(),
        title: '70B+',
    },
    {
        description: 'In digital Currency Exchange',
        key: makeID(),
        title: '70B+',
    },
];

export const featuresText = 'Features';
export const featuresDescriptionText = 'Why choose Multitraders';

export const howItWorksText = 'How it works';
export const howItWorksDescriptionText = 'It is really simple';
export const signUpInformationText = [
    `Multitraders is a full-featured spot trading platform for the
    major cryptocurrencies such as BITCOIN, Manera, Zcash.`,
    `It does not matter, whether you are new to cryptocurrency
    trading world or a high experienced trader, Multitraders is a highly secured,
    simple and fast trading environment.`,
];
export const wayTitleMultitradersText = 'MULTITRADERS : The way of the wise';
export const howItWorksStepsText = [
    {
        active: true,
        key: makeID(),
        title: 'Create an account',
    },
    {
        active: false,
        key: makeID(),
        title: 'Make a deposit',
    },
    {
        active: false,
        key: makeID(),
        title: 'Buy and Sell cryptocurrencies',
    },
    {
        active: false,
        key: makeID(),
        title: 'Withdraw your funds with just one click',
    },
];

export const pressTitleText = 'Press';
export const pressDescriptionText = 'The best companies are talking';

export const BoundaryItemsText = [
    {
        image: pressImage,
        key: makeID(),
        link: '#',
        linkTitle: 'Link address',
        title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor',
    },
    {
        image: pressImage,
        key: makeID(),
        link: '#',
        linkTitle: 'Link address',
        title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor',
    },
    {
        image: pressImage,
        key: makeID(),
        link: '#',
        linkTitle: 'Link address',
        title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor',
    },
];
