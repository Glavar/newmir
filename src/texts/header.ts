import makeID from '_util/makeID';

export const titleMainPageText = 'Multitraders';
export const titleMainPageSiginText = `${titleMainPageText} - Sign In`;

export const headerMenuText = [
    {
        key: makeID(),
        link: '#',
        title: 'Home',
    },
    {
        key: makeID(),
        link: '#',
        title: 'Exchange/Trade',
    },
    {
        key: makeID(),
        link: '#',
        title: 'Wallet',
    },
    {
        key: makeID(),
        link: '#',
        title: 'Affiliate',
    },
    {
        key: makeID(),
        link: '#',
        title: 'F.A.Q.',
    },
];

export const logInText = 'Log In';
export const logInTitleText = 'Log In';
export const signUpText = 'SIGN UP NOW';
export const earlyAccessTitleText = 'Early access';

export const enterEmailText = 'Enter your email';
export const keepMeSignInText = 'Keep me sign in';
export const accessToMyAccountText = 'Access to my account';

export const searchTitleText = 'Multitraders Fast growing cryptocurrency';
export const searchDescriptionText = `Buy and Sell cryptocurrency with the best rate and the lowest fees.
Create a wallet easy, secure and fast.
Make money by referring your friends to Multitraders`;
export const searchButtonText = 'What are cryptocurrencies?';
