export const emailText = 'Email';
export const passwordText = 'Password';
export const passwordRepeatText = 'Repeat password';
export const goBackToHome = 'Go back to home';
