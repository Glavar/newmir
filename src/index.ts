import * as ReactDOM from 'react-dom';
import * as WebFont from 'webfontloader';
import Application from './application';

ReactDOM.render(Application(), document.getElementById('root'), () => {
    WebFont.load({
        google: {
            families: ['Roboto:300,400,500,700'],
        },
    });
});
