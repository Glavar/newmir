export enum ETextTypes {
    chapterType = 'chapterType',
    titleType = 'titleType',
}

export const EPageFormats = {
    custom: {
        height: 882,
        inch: 'px',
        width: 679,
    },
};
