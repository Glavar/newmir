import {BackIcon} from '_icon';
import {copyrightText} from '_text/footer';
import cx from 'classnames';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {staticStyle} from './style';

export interface IModalComponent {
    show?: boolean;
    children?: React.ReactNode;
    backTitle?: string;
    onBack?: () => void;
}

const ROOT_ELEMENT_ID = 'root';
const BLUR_CLASS_NAME = 'isBlured';

function BackButton({backTitle, onBack}) {
    return (
        <div className={staticStyle.blockBack} onClick={onBack}>
            <BackIcon className={staticStyle.blockBackIcon} />
            <span className={staticStyle.backTitle}>{backTitle}</span>
        </div>
    );
}

export default class ModalComponent extends React.Component<IModalComponent> {
    public static defaultProps: IModalComponent = {
        show: false,
    };

    private popupNode = null;
    private rootElement: HTMLElement = null;

    constructor(props) {
        super(props);
        this.rootElement = document.getElementById(ROOT_ELEMENT_ID);
    }

    public componentDidMount() {
        this.popupNode = document.createElement('div');
        document.body.appendChild(this.popupNode);
        this._render();
    }

    public componentDidUpdate() {
        this._render();
    }

    public componentWillUnmount() {
        ReactDOM.unmountComponentAtNode(this.popupNode);
        document.body.removeChild(this.popupNode);
    }

    public _render() {
        const {show, children, backTitle, onBack, ...otherProps} = this.props;
        const classCx = cx(staticStyle.modal, {
            [staticStyle.showModal]: show,
        });
        this.rootElement.classList.toggle(BLUR_CLASS_NAME, show);

        const isShowBackButton = !!backTitle && typeof onBack === 'function';

        return ReactDOM.render(
            <div className={classCx} {...otherProps} data-modal={true}>
                <div className={staticStyle.blurAfter} />
                {isShowBackButton && <BackButton backTitle={backTitle} onBack={onBack} />}
                <div className={staticStyle.footer}>{copyrightText}</div>
                <>{show && children}</>
            </div>,
            this.popupNode,
        );
    }

    public render() {
        return false;
    }
}
