import cx from 'classnames';
import * as React from 'react';
// import injectSheet from 'react-jss';
import {staticStyle} from './style';

interface IButton {
    className?: string | string[] | any;
    children?: React.ReactNode;
    [id: string]: any;
}

// @injectSheet(dynamicStyle)
class Button extends React.Component<IButton, any> {
    public render() {
        const {className, children, ...props} = this.props;
        return (
            <button className={cx(staticStyle.button, className)} {...props}>
                {children}
            </button>
        );
    }
}

export default Button;
