import cx from 'classnames';
import * as React from 'react';
import {staticStyle} from './style';

export interface IInput {
    placeholder?: string;
    className?: string;
    IconComponent?: any;

    [id: string]: any;
}

export default function InputComponent(props: IInput) {
    const {className, IconComponent, ...otherProps} = props;
    return (
        <div className={staticStyle.inputBlock}>
            {IconComponent && <IconComponent className={staticStyle.icon} />}
            <input className={cx(staticStyle.input, className)} {...otherProps} />
        </div>
    );
}
