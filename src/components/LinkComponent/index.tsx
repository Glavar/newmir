import cx from 'classnames';
import * as React from 'react';
import {staticStyle} from './style';

export interface ILink {
    href: string;
    className?: string;
    children?: React.ReactNode;
}

const LinkComponent: React.SFC<ILink> = (props: ILink): JSX.Element => {
    const {href, children, className} = props;
    return (
        <a href={href} className={cx(staticStyle.link, className)}>
            {children}
        </a>
    );
};

export default LinkComponent;
