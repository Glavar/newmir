import cs from 'classnames';
import {Editor} from 'draft-js';
import {inject, observer} from 'mobx-react';
import * as React from 'react';

import noop from '_util/noop';
import {ETextTypes} from '../../constans';
import {staticStyle} from './style';
import {ITextEditorInject, ITextEditorProps} from './TextEditorComponent.interface';

function myBlockStyleFn(contentBlock) {
    const type = contentBlock.getType();
    if (type === ETextTypes.chapterType) {
        return staticStyle.chapterStyle;
    }
    if (type === ETextTypes.titleType) {
        return staticStyle.titleStyle;
    }
}

// export function removeBlockFromBlockMap(editorState: EditorState, blockKey: string) {
//     const contentState = editorState.getCurrentContent();
//     const blockMap = contentState.getBlockMap();
//     const newBlockMap = blockMap.remove(blockKey);
//     const newContentState = contentState.merge({
//         blockMap: newBlockMap,
//     }) as any;
//     return EditorState.push(editorState, newContentState, 'remove-range');
// }

const mapStateToProps = (state: any): ITextEditorInject => {
    return {
        setSelection: state.textStore.setSelection,
    };
};

@inject(mapStateToProps)
@observer
class TextEditorComponent extends React.Component<ITextEditorProps> {
    // private editor: any = React.createRef();
    // private timeout: NodeJS.Timer | number;
    // public componentDidUpdate(prevProps, prevState) {
    //     const {store} = this.props;
    //     if (store !== prevProps.store) {
    //         return this.setState({textState: store});
    //     }
    //     this.checkOnUpdateMuchText.call(this, prevState, prevProps);
    // }

    public handleClick = (event) => {
        event.preventDefault();
        event.stopPropagation();
        this.props.setSelection();
    }

    public render() {
        const {isView, store} = this.props;
        return (
            <div
                className={cs({
                    [staticStyle.isView]: isView,
                })}
                onMouseUpCapture={this.handleClick}
            >
                <Editor editorState={store} onChange={noop} blockStyleFn={myBlockStyleFn} readOnly={true} />
            </div>
        );
    }

    // private setChapterType = () => {
    //     const textState = RichUtils.toggleBlockType(this.state.textState, ETextTypes.chapterType);
    //     this.setState({textState}, this.checkOnMuchText);
    // }
    //
    // private setTitleType = () => {
    //     const textState = RichUtils.toggleBlockType(this.state.textState, ETextTypes.titleType);
    //     this.setState({textState}, this.checkOnMuchText);
    // }
    //
    // private handleChangeText = (textState) => {
    //     this.setState({
    //         textState,
    //         textState2: EditorState.createWithContent(
    //          ContentState.createFromText(textState.getCurrentContent().getPlainText())
    //         ),
    //     });
    // }

    // private checkOnUpdateMuchText(prevState: any) {
    //     const {textState} = this.state;
    //     const prevText = prevState.textState.getCurrentContent().getPlainText();
    //     const currentText = textState.getCurrentContent().getPlainText();
    //     if (prevText === currentText) {
    //         return;
    //     }
    //     this.checkOnMuchText();
    // }

    // private checkOnMuchText() {
    //     clearTimeout(this.timeout as number);
    //     this.timeout = setTimeout(() => {
    //         const {
    //             index,
    //             parentElement,
    //             textStore: {pushToNextPage},
    //         } = this.props;
    //         const parentSize = parentElement.getBoundingClientRect();
    //         const elements = getEditorNodes(this.editor.current.editorContainer);
    //         const indexesBlocks = findOutText(elements, parentSize);
    //         if (!indexesBlocks.length) {
    //             return;
    //         }
    //         const wordsOut = cropTextInNodes({
    //             elements,
    //             indexesBlocks,
    //             parentSize,
    //             textState: this.state.textState,
    //         });
    //         pushToNextPage(wordsOut, index);
    //     }, 0);
    // }
}

export default TextEditorComponent;
