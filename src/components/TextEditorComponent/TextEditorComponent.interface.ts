import {EditorState} from 'draft-js';
import {ReactNode} from 'react';

export interface ICropTextInNodesInterface {
    indexesBlocks: number[];
    elements: HTMLElement[];
    textState: any;
    parentSize: ClientRect;
}

export interface ITextEditorInject {
    setSelection?: () => void;
}

interface ITextEditorStoreProps {
    isView: boolean;
    store: EditorState;
    children?: ReactNode;
}

export type ITextEditorProps = ITextEditorStoreProps & ITextEditorInject;
