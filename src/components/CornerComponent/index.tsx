import cs from 'classnames';
import * as React from 'react';
import {staticStyle} from './style';

export enum ECornerTypes {
    checkbox = 'checkbox',
    radio = 'radio',
}

interface ICornerProps {
    type?: typeof ECornerTypes.checkbox | typeof ECornerTypes.radio;
    name: string;
    title: string;
    className?: any;
    [id: string]: any;
}

interface ICornerState {
    active: boolean;
    [id: string]: any;
}

const defaultProps: ICornerProps = {
    name: null,
    title: '',
    type: ECornerTypes.checkbox,
};

class CornerComponent extends React.PureComponent<ICornerProps, ICornerState> {
    public static defaultProps = defaultProps;

    constructor(props) {
        super(props);

        this.state = {
            active: false,
        };
    }

    public render() {
        const {type, title, className} = this.props;
        const cornerClassName = cs(
            staticStyle.control,
            {
                [staticStyle.controlCheckbox]: type === ECornerTypes.checkbox,
                [staticStyle.controlRadio]: type === ECornerTypes.radio,
            },
            className,
        );
        return (
            <label className={cornerClassName}>
                <>{title}</>
                <input type={type} name={name} checked={this.state.active} onChange={this.toggleChangeActive} />
                <div className={staticStyle.controlIndicator} />
            </label>
        );
    }

    private toggleChangeActive = () => {
        this.setState(({active}) => ({active: !active}));
    }
}

export default CornerComponent;
