import cx from 'classnames';
import * as React from 'react';
import {staticStyle} from './style';

export function TableComponent(props) {
    return <div {...props} />;
}

export function TableRowComponent({className, ...props}: any) {
    return <div className={cx(staticStyle.row, className)} {...props} />;
}

export function TableCellComponent({className, ...props}: any) {
    return <div className={cx(staticStyle.cell, className)} {...props} />;
}
