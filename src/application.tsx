import {MainBlock} from '_block';
import {titleMainPageText} from '_text/header';
import {Provider} from 'mobx-react';
import * as React from 'react';
import {Helmet} from 'react-helmet';

import * as store from './store';

export default function Application() {
    return (
        <Provider {...store}>
            <>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{titleMainPageText}</title>
                    <meta name="author" content="Ihor Haiduk" />
                </Helmet>
                {/*<HeaderBlock/>*/}
                <MainBlock />
                {/*<FooterBlock/>*/}
            </>
        </Provider>
    );
}
