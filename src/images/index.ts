/* tslint:disable: no-var-requires */
export const currency1Image = require('./currency/1.png');
export const currency2Image = require('./currency/2.png');
export const currency3Image = require('./currency/3.png');
export const currency4Image = require('./currency/4.png');
export const currency5Image = require('./currency/5.png');
export const currency6Image = require('./currency/6.png');
export const currency7Image = require('./currency/7.png');
export const feature1Image = require('./feature/1.png');
export const howItWorksImage = require('./howItWorks.png');
export const mapLeftImage = require('./map-left.png');
export const mapRightImage = require('./map-right.png');
export const pressImage = require('./press.png');
