import * as React from 'react';

import {EditorStore} from './components/EditorStore';
import {PagesContainer} from './components/PagesContainer';
import {staticStyle} from './style';

export default function MainBlock() {
    return (
        <main className={staticStyle.main}>
            <PagesContainer />
            <EditorStore />
        </main>
    );
}
