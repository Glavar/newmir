import TextEditorComponent from '_component/TextEditorComponent';
import {observer} from 'mobx-react';
import * as React from 'react';
import {PageDrawComponent} from './components/PageDrawComponent';
import {staticStyle} from './style';

@observer
export class PageBlock extends React.Component<any, any> {
    private pageRef = React.createRef() as any;

    constructor(props) {
        super(props);
        this.state = {
            parentElement: null,
        };
    }

    // public componentDidMount() {
    //     this.setState({
    //         parentElement: this.pageRef.current,
    //     });
    // }

    public render() {
        const {store} = this.props;
        return (
            <div className={staticStyle.page} ref={this.pageRef}>
                <TextEditorComponent store={store} isView={true} />
                <PageDrawComponent />
            </div>
        );
    }
}
