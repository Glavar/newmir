import {Layer, Line, Stage} from 'konva';
import {observer} from 'mobx-react';
import * as React from 'react';
import {RefObject} from 'react';
import {staticStyle} from '../style';

const propsDot = {
    dash: [6, 6],
    lineCap: 'round',
    lineJoin: 'round',
    stroke: '#c4c4c4',
    strokeWidth: 1.5,
};

@observer
export class PageDrawComponent extends React.Component<any, any> {
    private pageDrawRef: RefObject<null> = React.createRef();

    public componentDidMount() {
        const stage = new Stage({
            container: this.pageDrawRef.current,
            height: 882,
            width: 679,
        });
        const layer = new Layer();

        const topLine = new Line({...propsDot, points: [0, 24, 679, 24]});
        const rightLine = new Line({...propsDot, points: [655, 0, 655, 882]});
        const bottomLine = new Line({...propsDot, points: [0, 858, 679, 858]});
        const leftLine = new Line({...propsDot, points: [24, 0, 24, 882]});

        layer.add(topLine);
        layer.add(rightLine);
        layer.add(bottomLine);
        layer.add(leftLine);
        setTimeout(() => {
            stage.add(layer);
        }, 2000);
    }

    public render() {
        return <div ref={this.pageDrawRef} className={staticStyle.canvas} />;
    }
}
