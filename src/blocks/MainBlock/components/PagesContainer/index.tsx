import {inject, observer} from 'mobx-react';
import * as React from 'react';
import {PageBlock} from '../PageBlock';
import {staticStyle} from './style';

const mapStateToProps = (state: any): any => {
    return {
        pagesTextState: state.textStore.pagesTextState,
    };
};

@inject(mapStateToProps)
@observer
export class PagesContainer extends React.Component<any> {
    public render() {
        return (
            <div className={staticStyle.container}>
                {this.props.pagesTextState.map(({key, text}, index) => (
                    <PageBlock key={key} store={text} index={index} />
                ))}
            </div>
        );
    }
}
