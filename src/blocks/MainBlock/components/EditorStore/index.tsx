import {Editor} from 'draft-js';
import {inject, observer} from 'mobx-react';
import * as React from 'react';

const mapStateToProps = (state: any): any => {
    const {
        textStore: {projectTextState, setProjectTextState},
    } = state;
    return {
        projectTextState,
        setProjectTextState,
    };
};

@inject(mapStateToProps)
@observer
export class EditorStore extends React.Component<any> {
    public render() {
        const {projectTextState, setProjectTextState} = this.props;
        return <Editor editorState={projectTextState} onChange={setProjectTextState} />;
    }
}
