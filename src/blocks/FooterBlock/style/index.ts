import * as gridStyle from '_rootStyle/grid.scss';
import * as staticStyle from './style.scss';

export {staticStyle, gridStyle};
