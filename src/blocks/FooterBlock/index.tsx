import * as React from 'react';

import {staticStyle} from './style';

const FooterBlock: React.SFC = (): JSX.Element => {
    return <div className={staticStyle.footer}>Footer</div>;
};

export default FooterBlock;
