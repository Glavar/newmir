import LazyContainer from '_component/LazyContainer';

export const HeaderBlock = LazyContainer(() => import('./HeaderBlock'));
export const MainBlock = LazyContainer(() => import('./MainBlock'));
export const FooterBlock = LazyContainer(() => import('./FooterBlock'));
