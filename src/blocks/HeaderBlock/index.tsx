import * as React from 'react';
import {staticStyle} from './style';

const HeaderBlock: React.SFC = (): JSX.Element => {
    return <header className={staticStyle.header}>Header</header>;
};

export default HeaderBlock;
