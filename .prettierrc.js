// .prettierrc.js
module.exports = {
    'tabWidth': 4,
    'trailingComma': 'es5',
    'printWidth': 120,
    'singleQuote': true,
    'semi': true,
    'arrowParens': 'always',
    'parser': 'typescript',
    'jsxBracketSameLine': false,
    'bracketSpacing': false,
}
