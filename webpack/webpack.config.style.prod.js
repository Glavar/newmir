const {resolve} = require('path');
const webpack = require('webpack');
const WebpackErrorNotificationPlugin = require('webpack-error-notification');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CssoWebpackPlugin = require('csso-webpack-plugin').default;

const ROOT_DIR = resolve(__dirname, '..');

module.exports = {

    target: "web",
    devtool: false,

    entry: {
        main: resolve(ROOT_DIR, 'src', 'style', 'main.scss'),
    },

    // Currently we need to add '.ts' to the resolve.extensions array.
    resolve: {
        modules: ['node_modules'],
        extensions: [".js", ".scss", ".css"],
        descriptionFiles: ['package.json'],
        moduleExtensions: ['-loader'],
        alias: {
            _rootStyle: resolve(ROOT_DIR, 'src', 'style'),
            _images: resolve(ROOT_DIR, 'src', 'images'),
        }
    },

    output: {
        path: resolve(ROOT_DIR, 'dist', 'style'),
    },

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.scss$/,
                use: "source-map-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: false,
                                modules: true,
                                namedExport: true,
                                camelCase: true,
                                localIdentName: '[hash:base64:5]',
                                importLoaders: 3
                            }
                        },
                        'group-css-media-queries-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: false,
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: false
                            }
                        }
                    ]
                }),
                exclude: /node_modules/,
            }
        ]
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new WebpackErrorNotificationPlugin(),

        new ExtractTextPlugin('default.css'),
        new CssoWebpackPlugin(),

        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),

    ],
};