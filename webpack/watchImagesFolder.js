const {resolve, basename, extname, dirname, join} = require('path');
const {writeFileSync, readFileSync} = require('fs');
const watch = require('node-watch');
const camelCase = require('camelcase');
const glob = require("glob");
const sass = require('node-sass');

/* ---------IMAGE WATCH------------- */

const imageFolder = resolve(__dirname, '..', 'src', 'images');

const readImageFolder = () => {
    const imagesPath = ['/* tslint:disable: no-var-requires */\n'];

    glob(join(imageFolder, '**', '*.png'), (er, files) => {
        files.forEach(file => {
            const shortPath = dirname(file).replace(imageFolder, '');
            const name = camelCase(shortPath.replace(/\//g, '') + basename(file, extname(file)));
            imagesPath.push(`export const ${name}Image = require('./${join('./', shortPath, basename(file))}');\n`);
        });
        writeFileSync(resolve(imageFolder, 'index.ts'), imagesPath.join(''));
    });

};

watch(imageFolder, (a, b) => {
    if (/\.png$/.test(b) === false) {
        return;
    }
    console.log('image update start');
    readImageFolder();
    console.log('image update end');
});

readImageFolder();

/* ---------STYLE WATCH------------- */

const filenameToTypingsFilename = (filename) => {
    const dirName = dirname(filename);
    const baseName = basename(filename);
    return resolve(dirName, `${baseName}.d.ts`);
};

const cssModuleToNamedExports = (cssModuleKeys) => {
    return cssModuleKeys
        .filter((v, i, a) => a.indexOf(v) === i)
        .sort()
        .map((key) => `export const ${camelCase(key)}: string;`)
        .join('\n');
};

const styleFolder = resolve(__dirname, '..', 'src');

const readStyleFolder = () => {
    glob(join(styleFolder, '**', '*.scss'), (er, files) => {
        files.forEach(fileName => {
            const cssModuleInterfaceFilename = filenameToTypingsFilename(fileName);
            const content = readFileSync(fileName).toString();

            const keyRegex = /\.([a-z][a-z0-9-_]+)/ig;
            let match;
            const cssModuleKeys = [];

            while (match = keyRegex.exec(content)) {
                if (cssModuleKeys.indexOf(match[1]) < 0) {
                    cssModuleKeys.push(match[1]);
                }
            }

            writeFileSync(cssModuleInterfaceFilename, cssModuleToNamedExports(cssModuleKeys) + '\n');
        });
    });

};

watch(styleFolder, {recursive: true}, function (a, b) {
    if (/\.scss$/.test(b) === false) {
        return;
    }
    console.log('style update start');
    readStyleFolder();
    console.log('style update end');
});

readStyleFolder();






