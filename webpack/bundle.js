"use strict";

const {resolve} = require('path');
const {readdirSync} = require('fs');
const ROOT_DIR = resolve(__dirname, '..');

Object.defineProperty(exports, "__esModule", {
    value: true
});

const bundle = [
    resolve(ROOT_DIR, 'src', 'index.ts'),
    resolve(ROOT_DIR, 'src', 'application.tsx'),
    resolve(ROOT_DIR, 'src', 'style', 'grid.scss'),
    resolve(ROOT_DIR, 'src', 'icons', 'index.ts'),
    resolve(ROOT_DIR, 'src', 'images', 'index.ts'),
    ...readdirSync(resolve(ROOT_DIR, 'src', 'components'))
        .map((file) => resolve(ROOT_DIR, 'src', 'components', file, 'index.tsx')),
    ...readdirSync(resolve(ROOT_DIR, 'src', 'utils'))
        .map((file) => resolve(ROOT_DIR, 'src', 'utils', file))
];

exports.productionBundle = bundle;

exports.developmentBundle = [
    'react-hot-loader/babel',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    ...bundle
];

exports.vendor = [
    'react',
    'react-dom',
    'draft-js',
    // 'react-jss',

    'mobx',
    'mobx-react',
    'react-helmet',

    // 'jss',
    // 'jss-nested',
    // 'jss-props-sort',
    // 'jss-vendor-prefixer',

    'classnames',
    'webfontloader'
];

exports.lang = readdirSync(resolve(ROOT_DIR, 'src', 'texts'))
    .map((file) => resolve(ROOT_DIR, 'src', 'texts', file));
