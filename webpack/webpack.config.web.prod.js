const {resolve} = require('path');
const webpack = require('webpack');
const WebpackErrorNotificationPlugin = require('webpack-error-notification');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');
const {CheckerPlugin} = require('awesome-typescript-loader');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const CssoWebpackPlugin = require('csso-webpack-plugin').default;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const ROOT_DIR = resolve(__dirname, '..');
const {productionBundle: bundle, vendor, lang} = require('./bundle');

module.exports = {

    target: "web",
    devtool: false,

    entry: {
        vendor,
        bundle,
        lang,
    },

    // Currently we need to add '.ts' to the resolve.extensions array.
    resolve: {
        modules: ['node_modules'],
        extensions: [".ts", ".tsx", ".js", ".scss", ".css"],
        descriptionFiles: ['package.json'],
        moduleExtensions: ['-loader'],
        alias: {
            _rootStyle: resolve(ROOT_DIR, 'src', 'style'),

            _block: resolve(ROOT_DIR, 'src', 'blocks', 'index.ts'),
            _component: resolve(ROOT_DIR, 'src', 'components'),
            _icon: resolve(ROOT_DIR, 'src', 'icons', 'index.ts'),
            _images: resolve(ROOT_DIR, 'src', 'images'),
            _store: resolve(ROOT_DIR, 'src', 'store'),
            _image: resolve(ROOT_DIR, 'src', 'images', 'index.ts'),
            _text: resolve(ROOT_DIR, 'src', 'texts'),
            _util: resolve(ROOT_DIR, 'src', 'utils'),
        }
    },

    output: {
        path: resolve(ROOT_DIR, 'dist'),
        filename: '[name].js',
        publicPath: '/',
        chunkFilename: '[name].js?v=[chunkhash]',
        libraryTarget: 'umd'
    },

    stats: {
        colors: true,
        modules: true,
        reasons: true,
        errorDetails: true
    },

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: false,
                            modules: true,
                            namedExport: true,
                            camelCase: true,
                            localIdentName: '[hash:base64:5]',
                            importLoaders: 3
                        }
                    },
                    'group-css-media-queries-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: false,
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: false
                        }
                    }
                ],
                include: resolve(ROOT_DIR, 'src')
            },
            {
                test: /\.ts(x?)$/,
                use: [
                    {
                        loader: 'awesome-typescript-loader'
                    }
                ],
                include: [
                    resolve(ROOT_DIR, 'src')
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {}
                    }
                ]
            }
        ]
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    chunks: "initial",
                    name: "vendor",
                    priority: 10,
                    enforce: true
                }
            }
        },
        occurrenceOrder: true // To keep filename consistent between different modes (for example building only)
    },

    plugins: [

        new WebpackErrorNotificationPlugin(),

        new webpack.DefinePlugin({
            'process.env': {
                BROWSER: JSON.stringify(true),
                NODE_ENV: JSON.stringify('production')
            }
        }),

        new CheckerPlugin(),

        new LodashModuleReplacementPlugin({
            'flattening': true,
        }),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new CssoWebpackPlugin(),
        new UglifyJsPlugin(),
        //     parallel: true,
        //     sourceMap: false,
        //     uglifyOptions: {
        //         ecma: 8,
        //         ie8: false,
        //         warnings: false,
        //         output: {
        //             beautify: false,
        //             keep_quoted_props: true,
        //             shebang: false,
        //             comments: false,
        //         },
        //         compress: {
        //             properties: true,
        //             dead_code: true,
        //             drop_debugger: true,
        //             unsafe_math: true,
        //             conditionals: true,
        //             loops: true,
        //             if_return: true,
        //             inline: true,
        //             collapse_vars: true,
        //             reduce_vars: true,
        //             drop_console: true,
        //             passes: 5,
        //             keep_infinity: true,
        //         }
        //     },
        // }),

        // new BundleAnalyzerPlugin({
        //     analyzerMode: 'server',
        //     analyzerHost: '127.0.0.1',
        //     analyzerPort: 8001,
        //     reportFilename: 'report.html',
        //     defaultSizes: 'parsed',
        //     openAnalyzer: false,
        //     generateStatsFile: false,
        //     statsFilename: 'stats.json',
        //     statsOptions: null,
        //     logLevel: 'info'
        // })
    ],
};