const {resolve} = require('path');
const webpack = require('webpack');
const WebpackErrorNotificationPlugin = require('webpack-error-notification');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');
const {CheckerPlugin} = require('awesome-typescript-loader');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

const sourceMap = true;
const ROOT_DIR = resolve(__dirname, '..');
const {developmentBundle: bundle, vendor, lang} = require('./bundle');

module.exports = {

    target: "web",
    devtool: 'source-map',

    entry: {
        vendor,
        bundle,
        lang,
    },

    // Currently we need to add '.ts' to the resolve.extensions array.
    resolve: {
        modules: ['node_modules'],
        extensions: [".ts", ".tsx", ".js", ".scss", ".css"],
        descriptionFiles: ['package.json'],
        moduleExtensions: ['-loader'],
        alias: {
            _rootStyle: resolve(ROOT_DIR, 'src', 'style'),

            _block: resolve(ROOT_DIR, 'src', 'blocks', 'index.ts'),
            _component: resolve(ROOT_DIR, 'src', 'components'),
            _icon: resolve(ROOT_DIR, 'src', 'icons', 'index.ts'),
            _images: resolve(ROOT_DIR, 'src', 'images'),
            _store: resolve(ROOT_DIR, 'src', 'store', 'index.ts'),
            _stores: resolve(ROOT_DIR, 'src', 'store'),
            _image: resolve(ROOT_DIR, 'src', 'images', 'index.ts'),
            _text: resolve(ROOT_DIR, 'src', 'texts'),
            _util: resolve(ROOT_DIR, 'src', 'utils'),
        }
    },

    output: {
        path: resolve(ROOT_DIR, 'dist'),
        filename: '[name].js',
        publicPath: '/',
        chunkFilename: '[name].js',
        libraryTarget: 'umd'
    },

    stats: {
        colors: true,
        modules: true,
        reasons: true,
        errorDetails: true
    },

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.scss$/,
                use: "source-map-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap,
                            modules: true,
                            namedExport: true,
                            camelCase: true,
                            localIdentName: '[local]--[hash:base64:5]',
                            importLoaders: 3
                        }
                    },
                    {
                        loader: 'group-css-media-queries-loader',
                        options: { sourceMap }
                    },
                    {
                        loader: 'postcss-loader',
                        options: { sourceMap }
                    },
                    {
                        loader: 'sass-loader',
                        options: { sourceMap }
                    }
                ],
                include: resolve(ROOT_DIR, 'src')
            },
            {
                enforce: 'pre',
                test: /\.ts(x?)$/,
                use: "source-map-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.ts(x?)$/,
                use: [
                    {
                        loader: 'awesome-typescript-loader'
                    }
                ],
                include: [
                    resolve(ROOT_DIR, 'src')
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {}
                    }
                ]
            }
        ]
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    chunks: "initial",
                    name: "vendor",
                    priority: 10,
                    enforce: true
                }
            }
        },
        occurrenceOrder: true // To keep filename consistent between different modes (for example building only)
    },

    plugins: [

        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new WebpackErrorNotificationPlugin(),

        new webpack.DefinePlugin({
            'process.env': {
                BROWSER: JSON.stringify(true),
                NODE_ENV: JSON.stringify('development')
            }
        }),

        new CheckerPlugin(),

        new LodashModuleReplacementPlugin({
            'flattening': true,
        }),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

        new BundleAnalyzerPlugin({
            analyzerMode: 'server',
            analyzerHost: '127.0.0.1',
            analyzerPort: 8001,
            reportFilename: 'report.html',
            defaultSizes: 'parsed',
            openAnalyzer: false,
            generateStatsFile: false,
            statsFilename: 'stats.json',
            statsOptions: null,
            logLevel: 'info'
        })
    ],

    watch: true,

    devServer: {
        hot: true,

        contentBase: resolve(ROOT_DIR, 'dist'),
        publicPath: '/',
        host: "0.0.0.0",

        port: 3002,
        historyApiFallback: true,
        stats: {
            colors: true,
            chunks: false,
        },
        compress: true,
        disableHostCheck: true,
        headers: {'Access-Control-Allow-Origin': '*'},
        open: false
    },
};
