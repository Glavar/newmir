const parseVersion = require('parse-version');
const {writeFileSync} = require('fs');
const {resolve} = require('path');
const {version} = require('../package');
const versoionParse = parseVersion(version + '.0');
const newVersion = `${versoionParse.major}.${versoionParse.minor}.${versoionParse.patch + 1}`;

const indexText = (`<!doctype html>

<html lang="en">
<head>
    <link rel="stylesheet" href="/style/default.css?v=${newVersion}">
</head>

<body>
<div id="root"></div>
<script src="/vendor.js?v=${newVersion}" async></script>
<script src="/bundle.js?v=${newVersion}" async></script>
<script src="/lang.js?v=${newVersion}" async></script>
</body>
</html>`).replace(/[\r|\n]/igm, '').replace(/>\s+</igm, '><');

writeFileSync(resolve(__dirname, '..', 'dist', 'index.html'), indexText);
