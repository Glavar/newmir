module.exports = {
    plugins: [
        require('postcss-gradient-transparency-fix'),
        require('pixrem'),
        require('autoprefixer')({browsers:["last 2 version", "IE >= 11", "Safari >= 10"]}),
    ]
}
